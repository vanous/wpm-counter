# wpm-counter

wpm/cpm counter for a textarea but mainly for a text editor in Jitbit helpdesk.

Features:

- calculate and display current wpm/cpm
- show failed keys (hover over wpm)
- reset by clicking on wpm

Screenshots:

<img src="assets/ksnip_20210408-214812.png" alt="screenshot" height=50> <img src="assets/ksnip_20210408-214836.png" alt="screenshot" height=50>

Initial brainstorming:

- written using svelte (could use react, but this will be a small one-off project and will not grow bigger, so bringing in full react is not desirable)

  - [x] initial setup ready
  - [x] even using Svelte for this is too much, but the idea is to have a way to explore how to do a component based additions to existing websites


- try using sveltekit? it is in beta now, but not sure if it is suitable. could use just pain svelte, maybe with hmr, i have a project like this already, so can bootstrap it fast

  - [x] tested sveltekit, not needed here, because we have nollup hot reload
  - [ ] actually, i might still consider it, but it now include a router, which would need to be removed (this is probably easy)

- current implementation is not very good - it uses vanilla or jQ to get an element. Better would be to have the attachment shim to do a bit more -  detect if the needed page is loaded and the append our holder component there, because now we have svelte to just hold js and cannot use it for the UI at all... here are some resources for this:
  - https://davidtang.io/2020-01-22-adding-svelte-3-to-an-existing-application/
  - https://svelte.dev/docs#Client-side_component_API
  - https://github.com/lingtalfi/TheBar/blob/master/discussions/inject-svelte-in-existing-app.md

  - [x] this is done now. the shim does more and the svelte code is working as expected for a component based system


- css meta framework is an overkill here, but... bulma? :)

  - [x] tried Bulma, but it interfered with jitbit styling...


- explore how to hook it into jitbit helpdesk (this is how we attach to the editor `$(wswgEditor.getEditorDoc()).keydown(customFunction);)`)
  - [x] this is done now, by appending svelteholder
  - [x] and by appending the js bundle
  - complete shim:

  ```js
      let stickyHeader = $("#stickyHeader a.graybutton.scrollToTop").parent();
      if (stickyHeader != null){
        let container = document.createElement("div");
        container.id = "svelteholder";
        stickyHeader.append(container);
        let sveltetest = document.createElement("script");
        sveltetest.setAttribute("type", "text/javascript");
        sveltetest.setAttribute("async", "false");
        sveltetest.setAttribute("src", "https://spares.robe.cz/static/bundle.js");
        document.getElementsByTagName("head")[0].appendChild(sveltetest);
      }
    ```


- the actual calculation is really simple

  ```js
  cpm = Math.round((characterTyped / timeElapsed) * 60);
  wpm = Math.round((characterTyped / 5 / timeElapsed) * 60);
  ```

- provide result in tiny floating modal near the bottom of the screen
  - [x] it is n the sticky header

- do not count the time if pause longer then say 5sec
  - [x] done

- filter characters like arrow keys and so on
  - [x] done by using keypress rather then keydown 

- the current way of calculating seems to be too optimistic, or i don't know... `wpm` seem always quite good... is this real?
  - [x] eliminating the infinity on the start. Seems ok now.

- extra bonus - collect a list of frequently backspaced characters to show most failing keys
  - [x] done
