import { writable, get } from "svelte/store";

export const cpm = writable(0);
export const wpm = writable(0);

export let charactersTyped = 0;
export let timeElapsed;
export let lastTime;

const MAXTIME = 5000;

let lastChar = [];
let CHARSIZE = 5;
export let failingChars = writable({});
let lastCharWasBckspace = false;

export function keydown(event) {
  //special keys
  var keyID = event.keyCode;
  switch (keyID) {
    case 8:
      lastCharWasBckspace = true;
      break;
    default:
      break;
  }
}

function logFailedKey(key) {
  if (key == " ") {
    return;
  }
  let failingLocal = get(failingChars);
  if (failingLocal.hasOwnProperty(key)) {
    failingLocal[key] += 1;
  } else {
    failingLocal[key] = 1;
  }

  failingChars.set(
    Object.entries(failingLocal)
      .sort(([, a], [, b]) => b - a)
      .reduce((r, [k, v]) => ({ ...r, [k]: v }), {})
  );
}

export function keypress(event) {
  //normal keys
  let key = event.key;
  if (lastCharWasBckspace) {
    logFailedKey(key);
  }

  lastCharWasBckspace = false;
  let millis = new Date().getTime();

  if (lastTime != null && timeElapsed != null) {
    let timeDelta = millis - lastTime;
    if (timeDelta < MAXTIME) {
      timeElapsed = timeElapsed + timeDelta;
      console.log("elapsed " + timeElapsed);
    }
  } else {
    timeElapsed = 0;
  }
  lastTime = millis;
  charactersTyped += 1;
  calculate_wpm();
  calculate_cpm();
}

export function reset_wpm() {
  charactersTyped = 0;
  timeElapsed = 0;
  lastTime = null;
  failingChars.set({});
  lastCharWasBckspace = false;
  wpm.set(0);
  cpm.set(0);
}

function calculate_cpm() {
  if (timeElapsed > 0 && timeElapsed != null) {
    cpm.set(Math.round((charactersTyped / (timeElapsed / 1000)) * 60));
  }
}

function calculate_wpm() {
  if (timeElapsed > 0 && timeElapsed != null) {
    wpm.set(Math.round((charactersTyped / 5 / (timeElapsed / 1000)) * 60));
  }
}
